//
//  ViewController.swift
//  PosRep
//
//  Created by Andrew Ward on 11/10/17.
//  Copyright © 2017 Andrew Ward. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    @IBOutlet var controllerView: NSVisualEffectView!
    
    @IBOutlet weak var enrouteWpt: NSTextField!
    @IBOutlet weak var enrouteWptTime: NSTextField!
    
    @IBOutlet weak var nextWpt: NSTextField!
    @IBOutlet weak var nextWptTime: NSTextField!
    
    @IBOutlet weak var thirdWpt: NSTextField!
    
    @IBOutlet weak var alt: NSTextField!
    @IBOutlet weak var mach: NSTextField!
    
    @IBOutlet weak var outputPosRep: NSTextField!
    @IBOutlet weak var errorLabel: NSTextField!
    
    @IBOutlet weak var clipboardButton: NSButton!
    
    @IBAction func generatePosRep(_ sender: Any) {
        if enrouteWpt.stringValue == "" {
            errorLabel.stringValue = "Error! Enroute waypoint blank!"
            return
        }
        
        if enrouteWptTime.stringValue == "" {
            errorLabel.stringValue = "Error! Enroute waypoint ETA blank!"
            return
        }
        
        if nextWpt.stringValue == "" {
            errorLabel.stringValue = "Error! Next waypoint blank!"
            return
        }
        
        if nextWptTime.stringValue == "" {
            errorLabel.stringValue = "Error! Next waypoint ETA blank!"
            return
        }
        
        if thirdWpt.stringValue == "" {
            errorLabel.stringValue = "Error! Third waypoint blank!"
            return
        }
        
        if alt.stringValue == "" {
            errorLabel.stringValue = "Error! Altitude blank!"
            return
        }
        
        if mach.stringValue == "" {
            errorLabel.stringValue = "Error! Mach number blank!"
            return
        }
        
        outputPosRep.stringValue = stringBuilder()
        errorLabel.stringValue = ""
        clipboardButton.isHidden = false
        
    }
    
    func stringBuilder() -> String {
        let output = "\(enrouteWpt.stringValue.uppercased()) at \(enrouteWptTime.stringValue)z, Flight Level \(alt.stringValue), Mach .\(mach.stringValue). Estimating \(nextWpt.stringValue.uppercased()) at \(nextWptTime.stringValue). \(thirdWpt.stringValue.uppercased()) next."
        return output
    }
    
    @IBAction func copyToClipboard(_ sender: Any) {
        let pasteboard = NSPasteboard.general
        pasteboard.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
        pasteboard.setString(outputPosRep.stringValue, forType: NSPasteboard.PasteboardType.string)
        clipboardButton.title = "Copied!"
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.clipboardButton.isHidden = true
            self.clipboardButton.title = "Copy to Clipboard"
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        controllerView.material = NSVisualEffectView.Material.ultraDark
        controllerView.state = NSVisualEffectView.State.active
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

