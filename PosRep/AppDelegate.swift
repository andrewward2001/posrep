//
//  AppDelegate.swift
//  PosRep
//
//  Created by Andrew Ward on 11/10/17.
//  Copyright © 2017 Andrew Ward. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        guard let window = NSApp.windows.first else {return}
        window.appearance = NSAppearance(named: .vibrantDark)
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

